<?php

use yii\db\Migration;

/**
 * Handles adding rating to table `{{%photo}}`.
 */
class m190415_140943_add_rating_column_to_photo_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%photo}}', 'rating', $this->float()->notNull()->defaultValue(0)->after('total_rating'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%photo}}', 'rating');
    }
}
