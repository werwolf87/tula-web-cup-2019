<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%photo_tag}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%photo}}`
 * - `{{%tag}}`
 * - `{{%user}}`
 * - `{{%user}}`
 */
class m190408_141232_create_photo_tag_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%photo_tag}}', [
            'id' => $this->primaryKey(),
            'photo_id' => $this->integer()->notNull(),
            'tag_id' => $this->integer()->notNull(),
            'created_at' => $this->datetime()->notNull(),
            'updated_at' => $this->datetime()->notNull(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer()->notNull(),
        ]);

        // creates index for column `photo_id`
        $this->createIndex(
            '{{%idx-photo_tag-photo_id}}',
            '{{%photo_tag}}',
            'photo_id'
        );

        // add foreign key for table `{{%photo}}`
        $this->addForeignKey(
            '{{%fk-photo_tag-photo_id}}',
            '{{%photo_tag}}',
            'photo_id',
            '{{%photo}}',
            'id',
            'CASCADE'
        );

        // creates index for column `tag_id`
        $this->createIndex(
            '{{%idx-photo_tag-tag_id}}',
            '{{%photo_tag}}',
            'tag_id'
        );

        // add foreign key for table `{{%tag}}`
        $this->addForeignKey(
            '{{%fk-photo_tag-tag_id}}',
            '{{%photo_tag}}',
            'tag_id',
            '{{%tag}}',
            'id',
            'CASCADE'
        );

        // creates index for column `created_by`
        $this->createIndex(
            '{{%idx-photo_tag-created_by}}',
            '{{%photo_tag}}',
            'created_by'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-photo_tag-created_by}}',
            '{{%photo_tag}}',
            'created_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `updated_by`
        $this->createIndex(
            '{{%idx-photo_tag-updated_by}}',
            '{{%photo_tag}}',
            'updated_by'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-photo_tag-updated_by}}',
            '{{%photo_tag}}',
            'updated_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%photo}}`
        $this->dropForeignKey(
            '{{%fk-photo_tag-photo_id}}',
            '{{%photo_tag}}'
        );

        // drops index for column `photo_id`
        $this->dropIndex(
            '{{%idx-photo_tag-photo_id}}',
            '{{%photo_tag}}'
        );

        // drops foreign key for table `{{%tag}}`
        $this->dropForeignKey(
            '{{%fk-photo_tag-tag_id}}',
            '{{%photo_tag}}'
        );

        // drops index for column `tag_id`
        $this->dropIndex(
            '{{%idx-photo_tag-tag_id}}',
            '{{%photo_tag}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-photo_tag-created_by}}',
            '{{%photo_tag}}'
        );

        // drops index for column `created_by`
        $this->dropIndex(
            '{{%idx-photo_tag-created_by}}',
            '{{%photo_tag}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-photo_tag-updated_by}}',
            '{{%photo_tag}}'
        );

        // drops index for column `updated_by`
        $this->dropIndex(
            '{{%idx-photo_tag-updated_by}}',
            '{{%photo_tag}}'
        );

        $this->dropTable('{{%photo_tag}}');
    }
}
