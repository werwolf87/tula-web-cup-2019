<?php

use yii\db\Migration;

/**
 * Handles adding count_votes to table `{{%photo}}`.
 */
class m190415_140811_add_count_votes_column_to_photo_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%photo}}', 'count_votes', $this->integer()->notNull()->defaultValue(0)->after('desc'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%photo}}', 'count_votes');
    }
}
