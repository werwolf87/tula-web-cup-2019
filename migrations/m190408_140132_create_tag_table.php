<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%tag}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user}}`
 * - `{{%user}}`
 */
class m190408_140132_create_tag_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%tag}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(64)->notNull()->unique(),
            'created_at' => $this->datetime()->notNull(),
            'updated_at' => $this->datetime()->notNull(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer()->notNull(),
        ]);

        // creates index for column `created_by`
        $this->createIndex(
            '{{%idx-tag-created_by}}',
            '{{%tag}}',
            'created_by'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-tag-created_by}}',
            '{{%tag}}',
            'created_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `updated_by`
        $this->createIndex(
            '{{%idx-tag-updated_by}}',
            '{{%tag}}',
            'updated_by'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-tag-updated_by}}',
            '{{%tag}}',
            'updated_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-tag-created_by}}',
            '{{%tag}}'
        );

        // drops index for column `created_by`
        $this->dropIndex(
            '{{%idx-tag-created_by}}',
            '{{%tag}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-tag-updated_by}}',
            '{{%tag}}'
        );

        // drops index for column `updated_by`
        $this->dropIndex(
            '{{%idx-tag-updated_by}}',
            '{{%tag}}'
        );

        $this->dropTable('{{%tag}}');
    }
}
