<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%photo}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user}}`
 * - `{{%user}}`
 */
class m190408_141022_create_photo_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%photo}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'path' => $this->string(255)->notNull(),
            'desc' => $this->string(255)->notNull(),
            'created_at' => $this->datetime()->notNull(),
            'updated_at' => $this->datetime()->notNull(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer()->notNull(),
        ]);

        // creates index for column `created_by`
        $this->createIndex(
            '{{%idx-photo-created_by}}',
            '{{%photo}}',
            'created_by'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-photo-created_by}}',
            '{{%photo}}',
            'created_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `updated_by`
        $this->createIndex(
            '{{%idx-photo-updated_by}}',
            '{{%photo}}',
            'updated_by'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-photo-updated_by}}',
            '{{%photo}}',
            'updated_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-photo-created_by}}',
            '{{%photo}}'
        );

        // drops index for column `created_by`
        $this->dropIndex(
            '{{%idx-photo-created_by}}',
            '{{%photo}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-photo-updated_by}}',
            '{{%photo}}'
        );

        // drops index for column `updated_by`
        $this->dropIndex(
            '{{%idx-photo-updated_by}}',
            '{{%photo}}'
        );

        $this->dropTable('{{%photo}}');
    }
}
