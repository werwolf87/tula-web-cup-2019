<?php

use yii\db\Migration;

/**
 * Handles adding total_rating to table `{{%photo}}`.
 */
class m190415_140909_add_total_rating_column_to_photo_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%photo}}', 'total_rating', $this->integer()->notNull()->defaultValue(0)->after('count_votes'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%photo}}', 'total_rating');
    }
}
