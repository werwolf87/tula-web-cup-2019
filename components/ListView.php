<?php


namespace app\components;

use yii\helpers\Html;
use yii\helpers\Url;


class ListView extends \yii\widgets\ListView
{

    public $sizer;

    public $availableSizes = [10 => '10', 20 => '20', 50 => '50', 0 => 'Все'];

    public $activePageSizeCssClass = 'active';
    public $linkOptions = [];

    public $pagination;

    /**
     * Renders a section of the specified name.
     * If the named section is not supported, false will be returned.
     * @param string $name the section name, e.g., `{summary}`, `{items}`.
     * @return string|bool the rendering result of the section, or false if the named section is not supported.
     */
    public function renderSection($name)
    {
        switch ($name) {
            case '{sizer}':
                return $this->renderSizer();
            case '{summary}':
                return $this->renderSummary();
            case '{items}':
                return $this->renderItems();
            case '{pager}':
                return $this->renderPager();
            case '{sorter}':
                return $this->renderSorter();
            default:
                return false;
        }
    }

    /**
     * @return string
     */
    public function renderSizer()
    {

        $this->pagination = $this->dataProvider->getPagination();

        $buttons = [];
        $currentPageSize = $this->pagination->getPageSize();

        foreach ($this->availableSizes as $size => $label) {
            $buttons[] = $this->renderPageSizeButton($label, $size, $this->sizer['pageCssClass'], $size === $currentPageSize);
        }

        return Html::tag('ul', implode("\n", $buttons), $this->options);
    }

    /**
     * @param $label
     * @param $pageSize
     * @param $class
     * @param $active
     * @return mixed
     */
    protected function renderPageSizeButton($label, $pageSize, $class, $active)
    {
        $options = ['class' => $class === '' ? null : $class];

        if ($active) {
            Html::addCssClass($options, $this->activePageSizeCssClass);
        }

        $request = \Yii::$app->getRequest();
        $params = $request->getQueryParams();
        $params['per-page'] = $pageSize;

        return Html::tag('li', Html::a($label, Url::to(array_merge(['/site/index'], $params)), $this->sizer['linkOptions']), $options);
    }
}