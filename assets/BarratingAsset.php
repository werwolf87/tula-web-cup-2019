<?php

namespace app\assets;

use yii\web\AssetBundle;

class BarratingAsset extends AssetBundle
{
    public $sourcePath = '@bower/jquery-bar-rating/dist';

    public $css = [
        'themes/fontawesome-stars.css'
    ];

    public $js = [
        'jquery.barrating.min.js',
    ];
}