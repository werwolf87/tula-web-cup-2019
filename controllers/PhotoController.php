<?php

namespace app\controllers;

use app\models\PhotoRating;
use app\models\PhotoTag;
use app\models\Tag;
use Yii;
use app\models\Photo;
use app\models\PhotoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * PhotoController implements the CRUD actions for Photo model.
 */
class PhotoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Photo models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PhotoSearch();
        $params = Yii::$app->request->queryParams;
        $params[$searchModel->formName()]['created_by'] = Yii::$app->user->id;
        $dataProvider = $searchModel->search($params);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Photo model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Photo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Photo();

        if ($model->load(Yii::$app->request->post())) {

            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');

            if ($model->imageFile) {
                $model->upload();
            }

            if ($model->save()) {

                $this->assignTags($model);

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Photo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        foreach ($model->assignedTags as $assignedTag) {
            $model->tags .= $assignedTag->tag->name . ', ';
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $this->assignTags($model);

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Photo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Photo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Photo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Photo::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function assignTags($model)
    {

        PhotoTag::deleteAll(['photo_id' => $model->id]);

        $arTags = explode(',', $model->tags);

        foreach ($arTags as $tag) {
            $tag = trim($tag);

            if (strlen($tag)) {

                $modelTag = Tag::find()->where(['name' => $tag])->one();
                if ($modelTag !== null) {
                    $modelPhotoTag = new PhotoTag();
                    $modelPhotoTag->photo_id = $model->id;
                    $modelPhotoTag->tag_id = $modelTag->id;
                    $modelPhotoTag->save();
                } else {
                    $modelTag = new Tag();
                    $modelTag->name = $tag;

                    if ($modelTag->save()) {
                        $modelPhotoTag = new PhotoTag();
                        $modelPhotoTag->photo_id = $model->id;
                        $modelPhotoTag->tag_id = $modelTag->id;
                        $modelPhotoTag->save();
                    }
                }
            }
        }
    }

    public function actionRating($id, $value)
    {

        $value = intval($value) > 0 ? $value : 1;
        $model = $this->findModel($id);
        if ($model === null) {
            return false;
        }

        if (Yii::$app->user->isGuest) {
            $this->goHome();
        }

        $modelPhotoRating = PhotoRating::find()->where(['photo_id' => $id, 'user_id' => Yii::$app->user->id])->one();

        if ($modelPhotoRating !== null) {
            return;
        } else {
            $modelPhotoRating = new PhotoRating();
            $modelPhotoRating->photo_id = $id;
            $modelPhotoRating->user_id = Yii::$app->user->id;
            $modelPhotoRating->rating = $value;

            if ($modelPhotoRating->save()) {
                $model->count_votes += 1;
                $model->total_rating += $value;
                $model->rating = round($model->total_rating / $model->count_votes,  1);
                $model->save();
            }
        }
        return true;
    }
}
