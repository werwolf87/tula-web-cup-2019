$(function () {
    $('.rating').barrating({
        theme: 'fontawesome-stars',
        onSelect: function (value, text, event) {
            var el = this;
            var id = el.$elem.data('id');
            $.get('/photo/rating/?id=' + id + '&value=' + value, function (data) {
                console.log(data);
            })
        }
    });
});