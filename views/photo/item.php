<?php

use yii\helpers\Html;

?>

<div class="card mb-4 shadow-sm">
    <?= Html::a(Html::img($model->path, [
            'class' => 'bd-placeholder-img card-img-top',
            'alt' => $model->name,
            'title' => $model->name]
    ), $model->path, [
        'data-fancybox' => 'gallery',
        'alt' => $model->name,
        'title' => $model->name
    ]);
    ?>
    <div class="p-2">
        <div class="row align-items-center">
            <div class="col-6">
                <select id="rating-<?= $model->id; ?>" class="rating" data-id="<?= $model->id; ?>">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5" selected="">5</option>
                </select>
            </div>
            <div class="col-6 text-right">
                <span style="vertical-align: text-bottom;"><?= $model->rating; ?> (<?= $model->count_votes; ?>)
            </div>
        </div>
    </div>
</div>