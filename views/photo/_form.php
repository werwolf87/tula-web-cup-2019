<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\AutoComplete;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model app\models\Photo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="photo-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'imageFile')->fileInput() ?>

    <?if(!$model->isNewRecord) {
        echo  Html::img('/' . $model->path, ['width' => 128, 'height' => 'auto']);
    }?>
    <br />
    <?= $form->field($model, 'desc')->textarea(['maxlength' => true, 'rows' => 5]) ?>

    <script>
        function split(val) {
            return val.split(/,\s*/);
        }

        function extractLast(term) {
            return split(term).pop();
        }
    </script>

    <?= $form->field($model, 'tags', ['template' => "{label}\n{input}\n{hint}\n{error}<small class='text-muted'>введите слова или словосочетания, разделяя их запятыми</small>"])->widget(AutoComplete::class, [
        'clientOptions' => [
            'source' => new JsExpression("function( request, response ) {
                $.getJSON( '" . Url::to(['/tag/autocomplete']) . "', {
                term: extractLast( request.term )
                }, response );
            }"),
            'select' => new JsExpression("function(event, ui) {
                var terms = split( this.value );
                terms.pop();
                terms.push( ui.item.value );
                terms.push( \"\" );
                this.value = terms.join( \", \" );
                return false;
            }"),
            'search' => new JsExpression("function() {
                var term = extractLast(this.value);
                if ( term.length < 2 ) {
                    return false;
                }
            }"),
            'focus' => new JsExpression("function() {
                return false;
            }")
        ],
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
