<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\PhotoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Dev 2019';

use app\models\PhotoSearch;
use yii\data\ActiveDataProvider;
use app\components\ListView;
use yii\widgets\Pjax;

?>

<? Pjax::begin(); ?>
<?= ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '/photo/item',
    'itemOptions' => [
        'class' => 'col-12 col-sm-6',
    ],
    'pager' => [
        'options' => [
            'class' => 'pagination mt-2 mb-2',
        ],
        'pageCssClass' => 'page-item',
        'prevPageCssClass' => 'page-item',
        'disabledPageCssClass' => 'page-link',
        'linkOptions' => [
            'class' => 'page-link'
        ],
    ],
    'sizer' => [
        'pageCssClass' => 'page-item',
        'linkOptions' => [
            'class' => 'page-link'
        ],
    ],
    'sorter' => [
        'options' => [
            'class' => ''
        ]
    ],
    'options' => [
        'tag' => 'div',
        'class' => 'photo-list',
        'id' => 'photo-list',
    ],
    'layout' => "<div class='mb-3 d-flex justify-content-between align-items-center'><div class='sorter'>Сортировать по: {sorter}</div><div class='sizer'>Показывать {sizer}</div></div>\n<div class=\"row\">{items}</div>\n{pager}",
]); ?>
<? Pjax::end(); ?>
