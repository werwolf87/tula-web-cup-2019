<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\bootstrap4\Breadcrumbs;
use app\assets\AppAsset;
use app\models\Tag;

/*Yii::$app->assetManager->bundles = [
    'yii\bootstrap\BootstrapPluginAsset' => false,
    'yii\bootstrap\BootstrapAsset' => false
];*/

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<?php
NavBar::begin([
    'brandLabel' => Yii::$app->name,
    'brandUrl' => Yii::$app->homeUrl,
    'options' => [
        'class' => 'navbar navbar-dark bg-dark navbar-expand-lg',
    ],
]);

$menuItems = [
    ['label' => 'Главная', 'url' => ['/site/index']],
    ['label' => 'О проекте', 'url' => ['/site/about']]
];

if (Yii::$app->user->isGuest) {
    $menuItems[] = ['label' => 'Авторизация', 'url' => ['/site/login']];
    $menuItems[] = ['label' => 'Регистрация', 'url' => ['/site/signup']];
} else {
    $menuItems[] = '<li class="nav-item">'
        . Html::beginForm(['/site/logout'], 'post')
        . Html::submitButton(
            'Выйти (' . Yii::$app->user->identity->username . ')',
            ['class' => 'btn btn-link nav-link']
        )
        . Html::endForm()
        . '</li>';
}

echo Nav::widget([
    'options' => ['class' => 'navbar-nav ml-auto'],
    'items' => $menuItems
]);
NavBar::end();
?>

<main role="main">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-9">
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
                <?= Alert::widget() ?>
                <?= $content ?>
            </div>
            <div class="col-12 col-sm-3">
                <? if (!Yii::$app->user->isGuest): ?>
                    <?= Html::a('Добавить изображение', ['/photo/create'], ['class' => 'btn btn-primary mb-2']) ?>
                    <?= Html::a('Мои изображения', ['/photo/index'], ['class' => 'btn btn-primary']) ?>
                <? endif; ?>
                <br/>
                Теги:<br/>
                <?
                $arTags = \yii\helpers\ArrayHelper::map(Tag::find()->all(), 'id', 'name');
                foreach ($arTags as $id => $tag) { ?>
                    <a href="/?PhotoSearch[tag]=<?= $id; ?>" class="btn btn-outline-primary mb-1"><?= $tag; ?></a>
                <? } ?>
            </div>
        </div>
    </div>
</main>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Dev 2019</p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<? /*<footer class="text-muted">
    <div class="container">
        <p class="float-right">
            <a href="#">Back to top</a>
        </p>
        <p>Album example is © Bootstrap, but please download and customize it for yourself!</p>
        <p>New to Bootstrap? <a href="https://getbootstrap.com/">Visit the homepage</a> or read our <a href="/docs/4.3/getting-started/introduction/">getting started guide</a>.</p>
    </div>
</footer*/ ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
