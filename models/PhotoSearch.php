<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * PhotoSearch represents the model behind the search form of `app\models\Photo`.
 */
class PhotoSearch extends Photo
{

    public $tag;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'updated_by'], 'integer'],
            [['name', 'path', 'desc', 'created_at', 'updated_at', 'tag', 'rating'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Photo::find();

        // add conditions that should always apply here

        $pageSize = 20;
        if(isset($_REQUEST['per-page'])) {
            if($_REQUEST['per-page'] > 0) {
                $pageSize = $_REQUEST['per-page'];
            } else {
                $pageSize = false;
            }
        }


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'name',
                    'rating'
                ]
            ],
            'pagination' => [
                'pageSize' => $pageSize
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'rating' => $this->rating,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'path', $this->path])
            ->andFilterWhere(['like', 'desc', $this->desc]);

        if($this->tag) {
            $query->innerJoin('photo_tag', 'photo.id = photo_tag.photo_id')
                ->andFilterWhere(['like', 'photo_tag.tag_id', $this->tag]);
        }

        return $dataProvider;
    }
}
